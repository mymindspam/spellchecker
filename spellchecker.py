import os
import time
import itertools

AOEIUY = "aoeiuy"
DEFAULT_WORD_BASE_PATH = "/usr/share/dict/words"


def benchmark(method):
    def wrapper(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        print time.time() - ts
        return result

    return wrapper


class LoadDictionaryException(Exception):
    """Raise when my dictionary file path is wrong"""

    def __unicode__(self):
        return u"Wrong dictionary file path"


class SpellChecker(object):
    def __init__(self, word_base_file_path):
        if os.path.isfile(word_base_file_path):
            with open(word_base_file_path, 'r') as f:
                self.words_dictionary = set([line.strip() for line in f])
        else:
            raise LoadDictionaryException

    @benchmark
    def spell_check(self, word):
        """Spell checking

        :parameter word str - word to spell check
        :returns best spelling suggestion as a word
        :rtype str
        """
        variants = []
        dummy = [(k, 2) if len(list(v)) > 1 else (k, 1) for k, v in itertools.groupby(word)]
        combinations = itertools.product((1, 2), repeat=len([item for item in dummy if item[1] == 2]))
        for item in combinations:
            variant = ""
            index = 0
            for pair in dummy:
                if pair[1] == 2:
                    variant += pair[0] * int(item[index])
                    index += 1
                else:
                    variant += pair[0]
            variants.append(variant)
        for item in variants:
            if item in self.words_dictionary:
                return item
        for word in variants:
            aoeiuy = [index for index in range(len(word)) if word[index] in AOEIUY]
            aoeiuy_combinations = itertools.product(AOEIUY, repeat=len(aoeiuy))
            for item in aoeiuy_combinations:
                variant = ""
                vowel_index = 0
                for index in range(len(word)):
                    if word[index] in AOEIUY:
                        variant += item[vowel_index]
                        vowel_index += 1
                    else:
                        variant += word[index]
                if variant in self.words_dictionary:
                    return variant
        return "NO SUGGESTION"


def main():
    spellchecker = SpellChecker(DEFAULT_WORD_BASE_PATH)
    while True:
        print spellchecker.spell_check(raw_input("> "))


if __name__ == "__main__":
    main()